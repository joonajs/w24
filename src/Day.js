import React from 'react';

const Day = ({ month }) => {
    const weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    const date = new Date(`2021-${month}-01`);
    const weekday = weekdays[date.getDay()];

    return <div>Month {month}/2021 starts {weekday}</div>;
};

export default Day;
