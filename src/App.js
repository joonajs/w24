import React from 'react';
import Month from './Month';
import './App.css';

const App = () => {
    return (
        <div className='App'>
            <Month />
        </div>
    );
};

export default App;
